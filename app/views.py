# -*- coding: utf-8 -*-
import Crypto
import ast
import os,sys
import json
import random
from hashlib import md5
from hashlib import sha256
from app import app
from flask import request, redirect, render_template, url_for, flash
from json import dumps, load
from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os
import time
from urllib import urlencode
import requests
import json

@app.route('/liquechart',methods=['GET'])
def homelique():
    return render_template('showLique.html')

@app.route('/yobitchart',methods=['GET'])
def homeyobit():
    return render_template('showYobit.html')

@app.route('/lique_cup_sell',methods=['GET'])
def lique_cup():
    return render_template('lique_cup_sell.html')

@app.route('/bitcoinity_chart',methods=['GET'])
def home_bitcoinity():
    return render_template('show_bitcoinity.html')

@app.route('/bitcoinity_vshaped_chart',methods=['GET'])
def home_bitcoinity_vchart():
    return render_template('show_bitcoinity_vshaped.html')
